<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Hestec\ElementalExtensions\Dataobjects\Review;

class ElementReviews extends BaseElement
{

    private static $table_name = 'HestecElementReviews';

    private static $singular_name = 'Review';

    private static $plural_name = 'Reviews';

    private static $description = 'Element with reviews';

    private static $icon = 'font-icon-chat';

    private static $db = [
        'Content' => 'HTMLText'
    ];

    private static $has_many = array(
        'Reviews' => Review::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);

        $fields->addFieldToTab('Root.Main', $ContentField);

        if ($this->ID) {

            $ReviewsGridField = GridField::create(
                'Reviews',
                'Reviews',
                $this->Reviews(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $ReviewsGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'Reviews';
    }

    public function ReviewsRandom(){

        return $this->Reviews()->sort('RAND()');

    }

}