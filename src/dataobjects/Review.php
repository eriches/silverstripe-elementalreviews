<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Security\Permission;
use Hestec\ElementalExtensions\Elements\ElementReviews;

class Review extends DataObject {

    private static $table_name = 'HestecElementReview';

    private static $singular_name = 'Review';
    private static $plural_name = 'Reviews';

    private static $db = [
        'Name' => 'Varchar(100)',
        'Function' => 'Varchar(100)',
        'Title' => 'Varchar(255)',
        'Quote' => 'HTMLText',
        'Sort' => 'Int'
    ];

    private static $has_one = [
        'ElementReviews' => ElementReviews::class
    ];

    private static $summary_fields = [
        'Name',
        'Title'
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        /*$NameField = TextField::create('Name', "Name");
        $FunctionField = TextField::create('Function', "Function");
        $TitleField = TextField::create('Title', "Title");
        $QuoteField = HTMLEditorField::create('Quote', "Quote");
        $QuoteField->setRows(15);*/

        /*return new FieldList(
            $NameField,
            $FunctionField,
            $TitleField,
            $QuoteField
        );*/

        // Adding the Description field early will allow FluentField to decorate this with the appropriate
		// CSS classes.
		$this->beforeUpdateCMSFields(function($fields) {
            $QuoteField = HTMLEditorField::create('Quote', "Quote");
            $QuoteField->setRows(15);

            $fields->addFieldToTab('Root.Main', $QuoteField);
        });

		// The result of this call will have had beforeUpdateCMSFields then updateCMSFields called on it
		$fields = parent::getCMSFields();
		$fields->removeByName('Sort', true);
		return $fields;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
